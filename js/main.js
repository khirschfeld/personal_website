////////////////////
//PARTICLE EFFECTS//
////////////////////
particlesJS.load('particles-js', '../js/particle-config.json', function() {
    console.log('callback - particles.js config loaded');
});

/////////////////////
//TYPEWRITER EFFECT//
/////////////////////
const typewriter = function(txtElement, words, wait = 3000, speed){
    this.txtElement = txtElement;
    this.speed = speed;
    this.words = words;
    this.txt = ""; //currently displayed text
    this.wordIndex = 0; //index of current word
    this.wait = parseInt(wait, 10); //time to wait before deleting
    this.type();
    this.isDeleting = false; //stores if text is being deleted
}

//Type method
typewriter.prototype.type = function(){

    //Current word index
    const current = this.wordIndex % this.words.length;
    //Get full text of index
    const fullTxt = this.words[current];

    //check if deleting
    if(this.isDeleting){
        //Remove a char
        this.txt = fullTxt.substring(0, this.txt.length - 1);
    }else{
        //Add character
        this.txt = fullTxt.substring(0, this.txt.length + 1);
    }

    //insert txt into html
    this.txtElement.innerHTML =`<span class="txt">${this.txt}</span>`;

    // Type Speed
    let typespeed = this.speed;

    //make delete speed faster than write
    if(this.isDeleting){
        typespeed /= 2;
    }

    //if word is complete start new
    if(!this.isDeleting && this.txt === fullTxt){
        //pause at end
        typespeed = this.wait;
        this.isDeleting = true;

    }else if(this.isDeleting && this.txt === ""){
        //change word
        this.isDeleting = false;
        this.wordIndex++;
        //pause before resuming
        typespeed = 500;
    }

    setTimeout(() => this.type(), typespeed);
}

//Init app
function init(){
    //set copyright year
    document.getElementById("year").outerHTML = "&#169; " + new Date().getFullYear() + " nonamewastaken";

    //fetch data from html doc
    const txtElement = document.querySelector('.txt-type');
    const words = JSON.parse(txtElement.getAttribute('data-words'));
    const wait = txtElement.getAttribute('data-wait');
    const speed = txtElement.getAttribute('data-speed');

    //run Typewriter
    new typewriter(txtElement, words, wait, speed);
}

//Initialize on dom load
document.addEventListener("DOMContentLoaded", init);